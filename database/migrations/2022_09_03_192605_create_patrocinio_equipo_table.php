<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patrocinio_equipo', function (Blueprint $table) {
            $table->id();


            $table->unsignedBigInteger('equipo_id');
            $table->foreign('equipo_id')->references('id')->on('equipos')->onUpdate('cascade')->onDelete('cascade');

            $table->unsignedBigInteger('patrocinador_id');
            $table->foreign('patrocinador_id')->references('id')->on('patrocinadores')->onUpdate('cascade')->onDelete('cascade');

            $table->integer('monto')->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patrocinio_equipo');
    }
};
