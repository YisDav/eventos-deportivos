<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referis', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->foreign('id')->references('id')->on('personas')->onUpdate('cascade')->onDelete('cascade');

            $table->unsignedBigInteger('colegio_id');
            $table->foreign('colegio_id')->references('id')->on('colegios')->onUpdate('cascade')->onDelete('cascade');

            $table->integer('juegos_reglamentados')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referis');
    }
};
