<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividad_escenario', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('escenario_id');
            $table->foreign('escenario_id')->references('id')->on('escenarios')->onUpdate('cascade')->onDelete('cascade');

            $table->unsignedBigInteger('deporte_id');
            $table->foreign('deporte_id')->references('id')->on('deportes')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actividad_escenario');
    }
};
