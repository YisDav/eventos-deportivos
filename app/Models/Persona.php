<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'identificacion',
        'nombre',
        'apellido',
        'telefono',
        'direccion',
    ];

    public function participante() {
        return $this->hasOne(Participante::class, 'id');
    }

    public function referi() {
        return $this->hasOne(Referi::class, 'id');
    }

    public function getRol() {
        if($this->participante !== null)
            return $this->participante->rol();
        else if($this->referi !== null)
            return 'Réferi';
    }
}
