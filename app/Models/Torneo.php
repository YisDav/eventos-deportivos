<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Torneo extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nombre',
        'deporte_id',
        'municipio_id',
        'categoria'
    ];

    public function deporte()
    {
        return $this->belongsTo(Deporte::class);
    }

    public function municipio()
    {
        return $this->belongsTo(Municipio::class);
    }

    public function escenarios()
    {
        return $this->belongsToMany(Escenario::class, 'escenario_torneo')->withTimestamps();
    }

    public function equipos()
    {
        return $this->belongsToMany(Equipo::class, 'equipos_torneos')->withTimestamps();
    }

    public function getCategoria() {
        switch ($this->categoria) {
            case 1:
                return "Adultos masculino";
                break;
            case 2:
                return "Adultos femenino";
                break;
            case 3:
                return "Infantil masculino";
                break;
            case 4:
                return "Infantil femenino";
                break;
        }
    }
}
