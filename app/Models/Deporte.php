<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Deporte extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nombre',
        'descripcion',
    ];

    public function equipos() {
        return $this->hasMany(Equipo::class);
    }

    public function torneos()
    {
        return $this->hasMany(Torneo::class);
    }

    public function numero_participantes() {
        $participantes = 0;
        foreach ($this->equipos as $equipo) {
            $participantes+=$equipo->participantes->count();
        }
        return $participantes;
    }


    public function escenarios()
    {
        return $this->belongsToMany(Escenario::class, 'actividad_escenario')->withTimestamps();
    }
}
