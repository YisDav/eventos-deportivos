<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Equipo extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nombre',
        'deporte_id',
        'cantidad_integrantes'
    ];

    public function deporte() {
        return $this->belongsTo(Deporte::class);
    }

    public function participantes() {
        return $this->hasMany(Participante::class);
    }

    public static function updateAllEquiposParticipantes() {
        foreach(Equipo::all() as $equipo) {
            $equipo->cantidad_integrantes = $equipo->participantes->count();
            $equipo->save();
            $equipo->refresh();
        }
    }

    public function patrocinadores()
    {
        return $this->belongsToMany(Patrocinador::class, 'patrocinio_equipo')->withTimestamps()->withPivot('monto');
    }

    public function torneos()
    {
        return $this->belongsToMany(Torneo::class, 'equipos_torneos')->withTimestamps();
    }
}
