<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Deportista extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'participante_id',
        'rendimiento'
    ];

    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    public function participante() {
        return $this->morphOne(Participante::class, 'participa_en', 'participa_en_type', 'id');
    }
}
