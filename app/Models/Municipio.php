<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nombre',
        'departamento'
    ];

    public function torneos()
    {
        return $this->hasMany(Torneo::class);
    }

    public function escenarios()
    {
        return $this->hasMany(Escenario::class);
    }

    public function getDepartamento() {
        $value = '';
        switch ($this->departamento) {
            case 1:
                $value = "Amazonas";
                break;
            case 2:
                $value = "Antioquia";
                break;
            case 3:
                $value = "Arauca";
                break;
            case 4:
                $value = "Atlántico";
                break;
            case 5:
                $value = "Bogotá";
                break;
            case 6:
                $value = "Bolívar";
                break;
            case 7:
                $value = "Boyacá";
                break;
            case 8:
                $value = "Caldas";
                break;
            case 9:
                $value = "Caquetá";
                break;
            case 10:
                $value = "Casanare";
                break;
            case 11:
                $value = "Cauca";
                break;
            case 12:
                $value = "Cesar";
                break;
            case 13:
                $value = "Chocó";
                break;
            case 14:
                $value = "Córdoba";
                break;
            case 15:
                $value = "Cundinamarca";
                break;
            case 16:
                $value = "Guainía";
                break;
            case 17:
                $value = "Guaviare";
                break;
            case 18:
                $value = "Huila";
                break;
            case 19:
                $value = "La Guajira";
                break;
            case 20:
                $value = "Magdalena";
                break;
            case 21:
                $value = "Meta";
                break;
            case 22:
                $value = "Nariño";
                break;
            case 23:
                $value = "Norte de Santander";
                break;
            case 24:
                $value = "Putumayo";
                break;
            case 25:
                $value = "Quindío";
                break;
            case 26:
                $value = "Risaralda";
                break;
            case 27:
                $value = "San Andrés y Providencia";
                break;
            case 28:
                $value = "Santander";
                break;
            case 29:
                $value = "Sucre";
                break;
            case 30:
                $value = "Tolima";
                break;
            case 31:
                $value = "Valle del Cauca";
                break;
            case 32:
                $value = "Vaupés";
                break;
            case 33:
                $value = "Vichada";
                break;
        }
        return $value;
    }
}
