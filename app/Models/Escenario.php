<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Escenario extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nombre',
        'capacidad',
        'municipio_id'
    ];

    public function municipio()
    {
        return $this->belongsTo(Municipio::class);
    }

    public function deportes()
    {
        return $this->belongsToMany(Deporte::class, 'actividad_escenario')->withTimestamps();
    }

    public function torneos()
    {
        return $this->belongsToMany(Torneo::class, 'escenario_torneo')->withTimestamps();
    }
}
