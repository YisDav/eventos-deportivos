<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Participante extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'equipo_id',
        'participa_en_type'
    ];

    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    public function persona() {
        return $this->belongsTo(Persona::class, 'id');
    }

    public function equipo() {
        return $this->belongsTo(Equipo::class);
    }

    public function participa_en() {
        return $this->morphTo(__FUNCTION__, 'participa_en_type', 'id');
    }

    public function rol() {
        if ($this->participa_en_type == 'App\Models\Deportista')
            return 'Deportista';

        else if($this->participa_en_type == 'App\Models\Entrenador')
            return 'Entrenador';

        else return 'No participa';
    }
}
