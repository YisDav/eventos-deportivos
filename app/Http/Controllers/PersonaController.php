<?php

namespace App\Http\Controllers;

use App\Models\Persona;
use Illuminate\Http\Request;

class PersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $allPesonas = Persona::all()->sortBy('id');
        return view('personas.index', ['personas' => $allPesonas]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Persona  $persona
     * @return \Illuminate\Http\Response
     */
    public function show(Persona $persona)
    {
        //
        return view('personas.show', ['persona' => $persona]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Persona  $persona
     * @return \Illuminate\Http\Response
     */
    public function destroy(Persona $persona)
    {
        //
        $oldEquipo = $persona->participante->equipo;
        $persona->delete();
        \App\Models\Equipo::updateAllEquiposParticipantes();

        return redirect()->route('personas.index');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \App\Models\Persona
    */
    public static function store(Request $request)
    {
        PersonaController::validatePersonaFields($request);

        return Persona::create([
            'identificacion' => $request['identificacion'],
            'nombre' => $request['nombre'],
            'apellido' => $request['apellido'],
            'telefono' => $request['telefono'],
            'direccion' => $request['direccion']
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Persona  $persona
     * @return \App\Models\Persona
     */
    public static function update(Request $request, Persona $persona)
    {
        PersonaController::validatePersonaFields($request, $persona->id);

        $persona['identificacion'] = $request['identificacion'];
        $persona['nombre'] = $request['nombre'];
        $persona['apellido'] = $request['apellido'];
        $persona['telefono'] = $request['telefono'];
        $persona['direccion'] = $request['direccion'];
        $persona->save();

        return $persona;
    }

    /**
     * Common validate rules for PersonaController form validation
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Integer  $persona_id
     * @return \Illuminate\Http\Request
     */
    public static function validatePersonaFields(Request $request, $persona_id = 0) {
        return $request->validate([
            'identificacion' => "required|numeric|digits_between:1,12|unique:personas,identificacion,$persona_id",
            'nombre' => 'required|alpha|min:3|max:30',
            'apellido' => 'required|alpha|min:3|max:30',
            'telefono' => 'required|numeric|digits:10',
            'direccion' => 'required|string|min:10|max:50'
        ]);
    }
}
