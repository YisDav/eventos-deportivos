<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Participante;
use \App\Models\Equipo;

class ParticipanteController extends Controller
{
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\Participante
     */
    public static function store(Request $request)
    {
        $persona = PersonaController::store($request);

        ParticipanteController::validateParticipanteFields($request);
        $participante = Participante::create([
            'id' => $persona->id,
            'equipo_id' => $request['equipo_id'],
            'participa_en_type' => $request['participa_en_type']
        ]);
        Equipo::updateAllEquiposParticipantes();

        return $participante;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Participante  $participante
     * @return \App\Models\Participante
     */
    public static function update(Request $request, Participante $participante)
    {
        $persona = $participante->persona;
        PersonaController::update($request, $persona);

        ParticipanteController::validateParticipanteFields($request);

        $participante['equipo_id'] = $request['equipo_id'];
        $participante->save();
        $participante->refresh();
        Equipo::updateAllEquiposParticipantes();

        return $participante;
    }


    /**
     * Common validate rules for ParticipanteController validation
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Request
     */
    public static function validateParticipanteFields(Request $request) {
        return $request->validate([
            'equipo_id' => 'required|integer|exists:App\Models\Equipo,id'
        ]);
    }
}
