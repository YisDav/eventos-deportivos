<?php

namespace App\Http\Controllers;

use App\Models\Entrenador;
use Illuminate\Http\Request;

class EntrenadorController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('personas.entrenadores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request['participa_en_type'] = 'App\Models\Entrenador';

        $participante = ParticipanteController::store($request);
        $persona = $participante->persona;

        $validatedData = $this->validateEntrenadorField($request);
        Entrenador::create([
            'id' => $persona->id,
            'anios_experiencia' => $validatedData['anios_experiencia']
        ]);

        return redirect()->route('personas.show', $persona)->with('message', "El entrenador $persona->nombre $persona->apellido ha sido registrado");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Integer  $entrenador_id
     * @return \Illuminate\Http\Response
     */
    public function edit(Entrenador $entrenador)
    {
        //
        return view('personas.entrenadores.edit', ['entrenador' => $entrenador]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Entrenador  $entrenador
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Entrenador $entrenador)
    {
        //
        $participante = $entrenador->participante;
        $participante = ParticipanteController::update($request, $participante);

        $persona = $participante->persona;

        $validatedData = $this->validateEntrenadorField($request);
        $entrenador['anios_experiencia'] = $validatedData['anios_experiencia'];
        $entrenador->save();

        return redirect()->route('personas.show', $persona)->with('message', "La información del entrenador $persona->nombre $persona->apellido ha sido actualizada");
    }

    /**
     * Common validate rules for EntrenadorController form(s) validation
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Request
     */
    public static function validateEntrenadorField(Request $request) {
        $request->validate([
            'anios_experiencia' => 'integer|min:0',
        ]);
        return $request;
    }
}
