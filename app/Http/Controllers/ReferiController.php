<?php

namespace App\Http\Controllers;

use App\Models\Referi;
use Illuminate\Http\Request;

class ReferiController extends Controller
{
    /**
     * index and show methods are reused from PersonaController
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('personas.referis.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $persona = PersonaController::store($request);
        $validatedData = $this->validateReferiFields($request);
        $persona->referi()->create([
            'id' => $persona->id,
            'colegio_id' => $validatedData['colegio_id'],
            'juegos_reglamentados' => $validatedData['juegos_reglamentados'],
        ]);

        return redirect()->route('personas.show', $persona)->with('message', "El árbitro $persona->nombre $persona->apellido ha sido registrado");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Referi  $referi
     * @return \Illuminate\Http\Response
     */
    public function edit(Referi $referi)
    {
        //
        return view('personas.referis.edit', ['referi' => $referi]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Referi  $referi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Referi $referi)
    {
        //
        $persona = $referi->persona;
        PersonaController::update($request, $persona);
        $validatedData = $this->validateReferiFields($request);

        $referi->colegio_id             = $validatedData['colegio_id'];
        $referi->juegos_reglamentados   = $validatedData['juegos_reglamentados'];
        $referi->save();

        return redirect()->route('personas.show', $persona)->with('message', "La información del árbitro $persona->nombre $persona->apellido ha sido actualizada");
    }


    /**
     * Common validate rules for ReferiController validation
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Request
     */
    public static function validateReferiFields(Request $request) {
        return $request->validate([
            'colegio_id' => 'required|integer|exists:App\Models\Colegio,id',
            'juegos_reglamentados' => 'required|integer|min:0'
        ]);
    }
}
