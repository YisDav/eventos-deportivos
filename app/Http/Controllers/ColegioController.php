<?php

namespace App\Http\Controllers;

use App\Models\Colegio;
use Illuminate\Http\Request;

class ColegioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('colegios.index', ['colegios' => Colegio::all()->sortBy('id')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('colegios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $this->validateColegioFields($request);
        $colegio = Colegio::create($validatedData);
        return redirect()->back()->with('message', "El colegio arbitral $colegio->nombre ha sido creado");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function edit(Colegio $colegio)
    {
        //
        return view('colegios.edit', ['colegio' => $colegio]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Colegio $colegio)
    {
        //
        $validatedData = $this->validateColegioFields($request, $colegio->id);

        $colegio->nombre        = $validatedData['nombre'];
        $colegio->descripcion   = $validatedData['descripcion'];
        $colegio->save();

        return redirect()->route('colegios.edit', $colegio)->with('message', "El colegio arbitral $colegio->nombre ha sido actualizado");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Colegio $colegio)
    {
        //
        $response = "El colegio arbitral $colegio->nombre ha sido eliminado";

        foreach ($colegio->referis as $referi) $referi->persona->delete();
        $colegio->delete();

        return redirect()->route('colegios.index')->with('message', $response);
    }

    /**
     * Common validate rules for ColegioController form(s) validation
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Request
     */
    public static function validateColegioFields(Request $request, $colegio_id = 0) {
        return $request->validate([
            'nombre' => 'required|string|min:3|max:30|unique:App\Models\Colegio,nombre,'.$colegio_id,
            'descripcion' => 'nullable|string|max:80',
        ]);
    }
}
