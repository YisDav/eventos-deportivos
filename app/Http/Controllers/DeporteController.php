<?php

namespace App\Http\Controllers;

use App\Models\Deporte;
use Illuminate\Http\Request;

class DeporteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $allDeportes = Deporte::all()->sortBy('id');
        return view('deportes.index', ['deportes' => $allDeportes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('deportes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $this->validateDeporteFields($request);
        $deporte = Deporte::create($validatedData);
        return redirect()->route('deportes.create')->with('message', "El deporte $deporte->nombre se ha creado correctamente");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Deporte  $deporte
     * @return \Illuminate\Http\Response
     */
    public function edit(Deporte $deporte)
    {
        //
        return view('deportes.edit', ['deporte' => $deporte]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Deporte  $deporte
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Deporte $deporte)
    {
        //
        $validatedData = $this->validateDeporteFields($request, $deporte->id);

        $deporte->nombre            = $validatedData['nombre'];
        $deporte->descripcion       = $validatedData['descripcion'];
        $deporte->save();

        return redirect()->route('deportes.edit', $deporte)->with('message', "El deporte $deporte->nombre ha sido actualizado");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Deporte  $deporte
     * @return \Illuminate\Http\Response
     */
    public function destroy(Deporte $deporte)
    {
        //
        $response = "El deporte $deporte->nombre ha sido eliminado";
        $deporte->delete();
        return redirect()->route('deportes.index')->with('message', $response);
    }

    /**
     * Common validate rules for DeporteController form(s) validation
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Request
     */
    public static function validateDeporteFields(Request $request, $deporte_id = 0) {
        return $request->validate([
            'nombre' => 'required|string|min:3|max:30|unique:App\Models\Deporte,nombre,'.$deporte_id,
            'descripcion' => 'nullable|string|max:80',
        ]);
    }
}
