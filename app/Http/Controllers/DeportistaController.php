<?php

namespace App\Http\Controllers;

use App\Models\Deportista;
use Illuminate\Http\Request;

class DeportistaController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view("personas.deportistas.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request['participa_en_type'] = 'App\Models\Deportista';

        $participante = ParticipanteController::store($request);
        $persona = $participante->persona;

        $validatedData = $this->validateDeportistaField($request);
        Deportista::create([
            'id' => $persona->id,
            'rendimiento' => $validatedData['rendimiento']
        ]);

        return redirect()->route('personas.show', $persona)->with('message', "El deportista $persona->nombre $persona->apellido ha sido registrado");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Deportista  $deportista
     * @return \Illuminate\Http\Response
     */
    public function edit(Deportista $deportista)
    {
        //
        return view("personas.deportistas.edit", ["deportista" => $deportista]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Deportista  $deportista
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Deportista $deportista)
    {
        //
        $participante = $deportista->participante;
        $participante = ParticipanteController::update($request, $participante);

        $persona = $participante->persona;

        $validatedData = $this->validateDeportistaField($request);
        $deportista['rendimiento'] = $validatedData['rendimiento'];
        $deportista->save();

        return redirect()->route('personas.show', $persona)->with('message', "La información del deportista $persona->nombre $persona->apellido ha sido actualizada");
    }

    /**
     * Common validate rules for DeportistaController form(s) validation
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Request
     */
    public static function validateDeportistaField(Request $request) {
        $request->validate([
            'rendimiento' => 'nullable|integer|digits_between:0,3',
        ]);
        return $request;
    }
}
