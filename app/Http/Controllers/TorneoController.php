<?php

namespace App\Http\Controllers;

use App\Models\Torneo;
use App\Models\Municipio;
use App\Models\Equipo;
use Illuminate\Http\Request;

class TorneoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('torneos.index', ['torneos' => Torneo::all()->sortBy('id')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('torneos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // Create municipio if it doesn't exist
        // Disabled. Now it is needed create Municipio before create/modify a Torneo.
        // User can also create municipio in Escenario(s) views
        /*if($municipio == null) {
            $municipio = Municipio::create([
                'nombre' => $request['municipio_nombre']
            ]);
        }*/
        $validatedData = $this->validateTorneoFields($request);
        $municipio = Municipio::where('nombre', $request['municipio_nombre'])->first();
        $validatedData['municipio_id'] = $municipio->id;

        $torneo = Torneo::create($validatedData);
        foreach ($validatedData['ids_escenarios'] as $escenario_id)
            $torneo->escenarios()->attach($escenario_id);

        return redirect()->route('torneos.show', $torneo)->with('message', "Se ha creado el torneo: $torneo->nombre");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Torneo  $torneo
     * @return \Illuminate\Http\Response
     */
    public function show(Torneo $torneo)
    {
        //
        return view('torneos.show', ['torneo' => $torneo]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Torneo  $torneo
     * @return \Illuminate\Http\Response
     */
    public function edit(Torneo $torneo)
    {
        //
        return view('torneos.edit', ['torneo' => $torneo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Torneo  $torneo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Torneo $torneo)
    {
        //
        $validatedData = $this->validateTorneoFields($request, $torneo->id);
        $municipio = Municipio::where('nombre', $request['municipio_nombre'])->first();
        $validatedData['municipio_id'] = $municipio->id;

        $torneo->update($validatedData);
        $torneo->save();

        $torneo->escenarios()->detach();
        foreach ($validatedData['ids_escenarios'] as $escenario_id)
            $torneo->escenarios()->attach($escenario_id);
        $torneo->refresh();

        return redirect()->route('torneos.show', $torneo)->with('message', "Se ha actualizado el torneo: $torneo->nombre");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Torneo  $torneo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Torneo $torneo)
    {
        //
        $response = "Se ha eliminado el torneo $torneo";
        $torneo->delete();
        return redirect()->route('torneos.index')->with('message', $response);
    }

    /**
     * Common validate rules for TorneoController form(s) validation
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public static function validateTorneoFields(Request $request, $torneo_id = 0) {
        return $request->validate([
            'nombre' => 'required|string|min:3|max:30|unique:App\Models\Torneo,nombre,'.$torneo_id,
            'deporte_id' => 'required|integer|exists:App\Models\Deporte,id',
            'municipio_nombre' => 'nullable|exists:App\Models\Municipio,nombre',
            'ids_escenarios' => 'required|array|min:1',
            'categoria' => 'required|integer|min:1|max:4'
        ]);
    }

    public function registrarEquipo(Request $request, Torneo $torneo) {
        $validatedData = $this->validateEquipoFields($request);
        $torneo->equipos()->attach($validatedData['equipo_id']);
        $equipo = Equipo::find($validatedData['equipo_id']);
        return redirect()->back()->with('message', "Se ha registado al equipo $equipo->nombre en el torneo");
    }

    public function eliminarEquipo(Torneo $torneo, Equipo $equipo) {
        $torneo->equipos()->detach($equipo->id);
        return redirect()->back()->with('message', "Se ha eliminado al equipo $equipo->nombre del torneo");
    }

    public static function validateEquipoFields(Request $request) {
        return $request->validate([
            'equipo_id' => 'required|integer|exists:App\Models\Equipo,id|unique:equipos_torneos,equipo_id'
        ]);
    }
}
