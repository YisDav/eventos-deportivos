<?php

namespace App\Http\Controllers;

use App\Models\Patrocinador;
use App\Models\Equipo;
use Illuminate\Http\Request;

class PatrocinadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('patrocinadores.index', ['patrocinadores' => Patrocinador::all()->sortBy('id')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('patrocinadores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $this->validatePatrocinadorFields($request);
        $patrocinador = Patrocinador::create($validatedData);
        return redirect()->route('patrocinadores.show', $patrocinador)->with('message', "Se ha registrado el Patrocinador $patrocinador->razon_nombre");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Patrocinador  $patrocinador
     * @return \Illuminate\Http\Response
     */
    public function show(Patrocinador $patrocinador)
    {
        //
        return view('patrocinadores.show', ['patrocinador' => $patrocinador]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Patrocinador  $patrocinador
     * @return \Illuminate\Http\Response
     */
    public function edit(Patrocinador $patrocinador)
    {
        //
        return view('patrocinadores.edit', ['patrocinador' => $patrocinador]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Patrocinador  $patrocinador
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Patrocinador $patrocinador)
    {
        //
        $validatedData = $this->validatePatrocinadorFields($request);
        $patrocinador->update($validatedData);
        return redirect()->route('patrocinador.show', $patrocinador)->with('message', "Se ha actualizado la información del patrocinador $patrocinador->razon_nombre");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Patrocinador  $patrocinador
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patrocinador $patrocinador)
    {
        //
        $patrocinador->delete();
        return redirect()->route('patrocinadores.index')->with('message', 'Se ha eliminado el registro');
    }

    /**
     * Validate rules for PatrocinadorController form(s)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public static function validatePatrocinadorFields(Request $request, $patrocinador_id = 0) {
        return $request->validate([
            'identificacion' => 'required|string|min:3|max:30|unique:App\Models\Patrocinador,identificacion,'.$patrocinador_id,
            'razon_nombre' => 'required|string|min:3|max:30',
        ]);
    }

    /**
     * Create a new Sponsor by some Patrocinador
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Patrocinador  $patrocinador
     * @return \Illuminate\Http\Response
     */
    public function sponsor(Request $request, Patrocinador $patrocinador) {
        $validatedData = $this->validateSponsorshipFields($request);

        $patrocinador->patrocinios()->attach($validatedData['equipo_id'], ['monto' => $validatedData['monto']]);
        $equipo = Equipo::find($validatedData['equipo_id']);

        return redirect()->back()->with('message', "Se ha creado un nuevo patrocinio al equipo $equipo->nombre");
    }


    /**
     * UnSponsor a Team (Equipo) by a Patrocinador
     *
     * @param  \App\Models\Patrocinador  $patrocinador
     * @param  \App\Models\Equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function unsponsor(Patrocinador $patrocinador, Equipo $equipo) {
        $patrocinador->patrocinios()->detach($equipo->id);
        return redirect()->back()->with('message', "Se ha eliminado el patrocinio al equipo $equipo->nombre");
    }

    /**
     * Validate rules for new Sponsorship(s).
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public static function validateSponsorshipFields(Request $request) {
        return $request->validate([
            'equipo_id' => 'required|exists:App\Models\Equipo,id',
            'monto' => 'required|numeric|min:500000',
        ]);
    }
}
