<?php

namespace App\Http\Controllers;

use App\Models\Municipio;
use Illuminate\Http\Request;

class MunicipioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('municipios.index', ['municipios' => Municipio::all()->sortBy('id')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('municipios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $this->validateMunicipioFields($request);
        $municipio = Municipio::create($validatedData);
        return redirect()->back()->with('message', "El municipio $municipio->nombre ha sido registrado");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Municipio  $municipio
     * @return \Illuminate\Http\Response
     */
    public function edit(Municipio $municipio)
    {
        //
        return view('municipios.edit', ['municipio' => $municipio]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Municipio  $municipio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Municipio $municipio)
    {
        //
        $validatedData = $this->validateMunicipioFields($request, $municipio->id);

        $municipio->nombre = $validatedData['nombre'];
        $municipio->departamento = $validatedData['departamento'];
        $municipio->save();

        return redirect()->back()->with('message', "El municipio $municipio->nombre se ha actualizado correctamente");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Municipio  $municipio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Municipio $municipio)
    {
        //
        $response = "El municipio $municipio->nombre ha sido eliminado.";
        $municipio->delete();
        return redirect()->route('municipios.index')->with('message', $response);
    }

    /**
     * Common validate rules for MunicipioController form(s) validation
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Request
     */
    public static function validateMunicipioFields(Request $request, $municipio_id = 0) {
        return $request->validate([
            'nombre' => 'required|string|min:3|max:30|unique:App\Models\Municipio,nombre,'.$municipio_id,
            'departamento' => 'required|integer|min:1|max:33'
        ]);
    }
}
