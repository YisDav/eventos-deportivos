<?php

namespace App\Http\Controllers;

use App\Models\Equipo;
use App\Models\Deporte;
use Illuminate\Http\Request;

class EquipoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('equipos.index', ['equipos' => Equipo::all()->sortBy('id')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('equipos.create', ['deportes' => Deporte::all()->sortBy('id')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $this->validateEquipoFields($request);
        $equipo = Equipo::create($validatedData);
        return redirect()->route('equipos.show', $equipo)->with('message', "El Equipo $equipo->nombre ha sido creado");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function show(Equipo $equipo)
    {
        //
        return view('equipos.show', ['equipo' => $equipo]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function edit(Equipo $equipo)
    {
        //
        return view('equipos.edit', ['equipo' => $equipo, 'deportes' => Deporte::all()->sortBy('id')]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Equipo $equipo)
    {
        //
        $validatedData = $this->validateEquipoFields($request, $equipo->id);

        $equipo->nombre = $validatedData['nombre'];
        $equipo->deporte_id = $validatedData['deporte_id'];
        $equipo->save();

        return redirect()->route('equipos.show', $equipo)->with('message', "El equipo $equipo->nombre ha sido actualizado");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Equipo $equipo)
    {
        //
        $response = "El equipo $equipo->nombre ($equipo->id) ha sido eliminado";

        foreach ($equipo->participantes as $participante) {
            $participante->persona->delete();
        }

        $equipo->delete();

        return redirect()->route('equipos.index')->with('message', $response);
    }

    /**
     * Common validate rules for EquipoController form(s) validation
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Request
     */
    public static function validateEquipoFields(Request $request, $equipo_id = 0) {
        return $request->validate([
            'nombre' => 'required|string|min:3|max:30|unique:App\Models\Equipo,nombre,'.$equipo_id,
            'deporte_id' => 'required|integer|exists:App\Models\Deporte,id',
        ]);
    }
}
