<?php

namespace App\Http\Controllers;

use App\Models\Escenario;
use App\Models\Deporte;
use App\Models\Municipio;
use Illuminate\Http\Request;

class EscenarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('escenarios.index', ['escenarios' => Escenario::all()->sortBy('id')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('escenarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $municipio = Municipio::where('nombre', $request['municipio_nombre'])->first();

        //create municipio if it doesn't exist
        if($municipio == null) {
            $municipio = Municipio::create([
                'nombre' => $request['municipio_nombre']
            ]);
        }

        $request['municipio_id'] = $municipio->id;
        $validatedData = $this->validateEscenarioFields($request);

        $escenario = $municipio->escenarios()->create([
            'nombre' => $validatedData['nombre'],
            'capacidad' => $validatedData['capacidad'],
            'municipio_id' => $validatedData['municipio_id']
        ]);

        foreach ($validatedData['ids_deportes'] as $deporte_id)
            $escenario->deportes()->attach($deporte_id);

        return redirect()->route('escenarios.show', $escenario)->with('message', "Se ha creado el escenario: $escenario->nombre");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Escenario  $escenario
     * @return \Illuminate\Http\Response
     */
    public function show(Escenario $escenario)
    {
        //
        return view('escenarios.show', ['escenario' => $escenario]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Escenario  $escenario
     * @return \Illuminate\Http\Response
     */
    public function edit(Escenario $escenario)
    {
        //
        return view('escenarios.edit', ['escenario' => $escenario]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Escenario  $escenario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Escenario $escenario)
    {
        //
        $municipio = Municipio::where('nombre', $request['municipio_nombre'])->first();

        //create municipio if it doesn't exist
        if($municipio == null) {
            $municipio = Municipio::create([
                'nombre' => $request['municipio_nombre']
            ]);
        }

        $request['municipio_id'] = $municipio->id;
        $validatedData = $this->validateEscenarioFields($request);


        $escenario->nombre = $validatedData['nombre'];
        $escenario->capacidad = $validatedData['capacidad'];
        $escenario->municipio_id = $validatedData['municipio_id'];
        $escenario->save();

        $escenario->deportes()->detach();
        foreach ($validatedData['ids_deportes'] as $deporte_id)
            $escenario->deportes()->attach($deporte_id);
        $escenario->refresh();

        return redirect()->route('escenarios.show', $escenario)->with('message', "Se ha actualizado el escenario: $escenario->nombre");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Escenario  $escenario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Escenario $escenario)
    {
        //
        $response = "El escenario $escenario->nombre ha sido eliminado.";
        $escenario->delete();
        return redirect()->route('escenarios.index')->with('message', $response);
    }


    /**
     * Common validate rules for EscenarioController validation
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Request
     */
    public static function validateEscenarioFields(Request $request) {
        return $request->validate([
            'nombre' => 'required|string|min:3|max:30',
            'capacidad' => 'required|integer|min:1',
            'municipio_id' => 'required|exists:App\Models\Municipio,id',
            'ids_deportes' => 'required|array|min:1'
        ]);
    }

    // AJAX
    public function getEscenarios(Request $request) {
        $escenarios = Escenario::all();
        $municipio = Municipio::where('nombre', $request->input('municipioNombre'))->first();
        $escenarios_return = [];

        foreach ($escenarios as $escenario) {
            if($escenario->deportes()->where('deportes.id', $request['deporteID'])->exists())
                if($escenario->municipio->id == $municipio->id)
                    array_push($escenarios_return, $escenario);
        }

        return $escenarios_return;
    }
}
