<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});



Route::resource('personas/deportistas', 'App\Http\Controllers\DeportistaController')
    ->except(['index', 'show', 'destroy']);

Route::resource('personas/entrenadores', 'App\Http\Controllers\EntrenadorController')
    ->except(['index', 'show', 'destroy'])
    ->parameters([
        'entrenadores' => 'entrenador'
    ]);

Route::resource('personas/referis', 'App\Http\Controllers\ReferiController')
        ->except(['index', 'show', 'destroy']);

Route::resource('personas', 'App\Http\Controllers\PersonaController')
    ->only(['index', 'show', 'destroy']);



Route::resource('deportes', 'App\Http\Controllers\DeporteController')
    ->except(['show']);

Route::resource('equipos', 'App\Http\Controllers\EquipoController');

Route::resource('colegios', 'App\Http\Controllers\ColegioController')
    ->except(['show']);

Route::resource('municipios', 'App\Http\Controllers\MunicipioController')
    ->except(['show']);

Route::resource('torneos', 'App\Http\Controllers\TorneoController');

Route::post('torneos/{torneo}/registrar-equipo', 'App\Http\Controllers\TorneoController@registrarEquipo')
    ->name('torneo.registrarEquipo');

Route::delete('torneos/{torneo}/eliminar-equipo/{equipo}', 'App\Http\Controllers\TorneoController@eliminarEquipo')
    ->name('torneo.eliminarEquipo');

Route::resource('escenarios', 'App\Http\Controllers\EscenarioController');
Route::post('/get-escenarios', 'App\Http\Controllers\EscenarioController@getEscenarios');

Route::resource('patrocinadores', 'App\Http\Controllers\PatrocinadorController')
    ->parameters([
        'patrocinadores' => 'patrocinador'
    ]);

Route::post('patrocinadores/{patrocinador}/sponsor/', 'App\Http\Controllers\PatrocinadorController@sponsor')
    ->name('patrocinadores.sponsor');

Route::delete('patrocinadores/{patrocinador}/unsponsor/{equipo}', 'App\Http\Controllers\PatrocinadorController@unsponsor')
    ->name('patrocinadores.unsponsor');
