@php
    $torneoFix = isset($torneo) ? $torneo : null;

    function getInputvalue($inputName, $torneoFix) {
        if(old($inputName) !== null) {
            return old($inputName);
        }
        else if ($torneoFix !== null) {
            switch ($inputName) {
                case 'municipio':
                    return $torneoFix->municipio->nombre;
                    break;

                case 'ids_escenarios':
                    return $torneoFix->escenarios;
                    break;

                default:
                    return $torneoFix["$inputName"];
                    break;
            }
        }
        else return '';
    }
@endphp

<div class="row g-2 mb-2">
    <div class="col">
        <div class="form-floating">
            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre del torneo" value="{{ getInputvalue('nombre', $torneoFix) }}" required>
            <label for="nombre">Nombre del torneo:</label>
        </div>
    </div>
</div>

<div class="row g-2 mb-2">
    <div class="col">
        <div class="form-floating">
            <select class="form-select" id="deporte_id" name="deporte_id" required>
                <option disabled>Seleccionar deporte</option>
                @foreach (\App\Models\Deporte::all() as $deporte)
                    <option
                        value="{{ $deporte->id }}" {{ (isset($torneo) && $torneo->deporte_id == $deporte->id) ? 'selected' : '' }} >
                            {{ $deporte->nombre }}
                    </option>
                @endforeach
            </select>
            <label for="descripcion">Deporte que practica:</label>
        </div>
    </div>
</div>

<div class="row g-2 mb-2">
    <div class="col">
        <div class="form-floating">
            <input class="form-control" list="municipioDatalist" id="municipio_nombre" name="municipio_nombre" placeholder="Type to search..." value="{{ getInputValue('municipio', $torneoFix) }}">
            <datalist id="municipioDatalist">
                @foreach (\App\Models\Municipio::all() as $municipio)
                    <option value="{{ $municipio->nombre }}">{{ $municipio->nombre }} {{ ($municipio->getDepartamento() !== "") ? '('.$municipio->getDepartamento().')' : '' }}</option>
                @endforeach
            </datalist>
            <label for="municipio_nombre" class="form-label">Municipio</label>
        </div>
    </div>
</div>

<div class="row g-2 mb-2">
    <div class="col">
        <label for="ids_escenarios" class="form-label">Escenarios Relacionados:</label>
        <select class="form-select" multiple aria-label="multiple select escenarios" id="ids_escenarios" name="ids_escenarios[]" multiple required>
        </select>
    </div>
    <script defer>
        var deporteID = document.getElementById('deporte_id');
        var municipioNombre = document.getElementById('municipio_nombre');

        deporteID.onchange = function() {
            if(deporteID.value !== null && municipioNombre.value !== null && municipioNombre.value !== "")
                updateEscenarios();
        }

        municipioNombre.onchange = function() {
            if(deporteID.value !== null && municipioNombre.value !== null && municipioNombre.value !== "")
                updateEscenarios();
        }

        var escenariosList = document.getElementById("ids_escenarios");
        function updateEscenarios() {
            var deporteIDValue = deporteID.value;
            var municipioNombreValue = municipioNombre.value;

            var xhr = new XMLHttpRequest();
            var data = "deporteID="+deporteIDValue+"&municipioNombre="+municipioNombreValue+"";
            xhr.open('POST', '/get-escenarios?'+data);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.setRequestHeader('X-CSRF-TOKEN', document.querySelector('meta[name="csrf-token"]').content);
            xhr.onload = function() {
                var escenarios = JSON.parse(xhr.responseText);
                updateEscenariosList(escenarios);
            };
            xhr.send();
        }

        function updateEscenariosList(escenarios) {
            escenariosList.innerHTML =''
            if(escenarios.length == 0) {
                var option = document.createElement('option');
                option.textContent = "No existe escenario para ese Deporte / Municipio";
                option.setAttribute("disabled","");
                escenariosList.appendChild(option);
            }
            else {
                escenarios.forEach(escenario => {
                    var option = document.createElement('option');
                    option.value = escenario.id;
                    option.textContent = escenario.nombre;
                    escenariosList.appendChild(option);
                });
            }
        }
    </script>
</div>



<div class="row g-2 mb-2">
    <div class="col">
        <div class="form-floating">
            <select class="form-select" id="categoria" name="categoria" required>
                <option disabled>Seleccionar categoría</option>
                <option value="1" {{ (isset($torneo) && $torneo->categoria == 1) ? 'selected' : '' }}>Adultos Masculina</option>
                <option value="2" {{ (isset($torneo) && $torneo->categoria == 2) ? 'selected' : '' }}>Adultos Femenina</option>
                <option value="3" {{ (isset($torneo) && $torneo->categoria == 3) ? 'selected' : '' }}>Intantil Masculina</option>
                <option value="4" {{ (isset($torneo) && $torneo->categoria == 4) ? 'selected' : '' }}>Intantil Femenina</option>
            </select>
            <label for="categoria">Deporte que practica:</label>
        </div>
    </div>
</div>
