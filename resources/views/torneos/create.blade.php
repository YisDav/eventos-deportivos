@extends('layout.general')
@section('page-title', "Registrar Torneo")

@section('page-title-centered', 'Registrar nuevo torneo')

@section('page-content')

    <div class="create-deportista-form">
        @component('components.form')
            @slot('action', route('torneos.store'))

            @slot('method', "POST")

            @slot('form_content')
                @component('torneos.components.fields')
                @endcomponent

                <div class="row">
                    <div class="col d-grid">
                        <button class="btn btn-primary" type="submit">Registrar</button>
                    </div>
                </div>
            @endslot
        @endcomponent
    </div>

@endsection
