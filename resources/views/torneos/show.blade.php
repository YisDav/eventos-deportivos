@extends('layout.general')

@section('page-title', "Torneo: $torneo->nombre")


@section('page-title-centered', 'Información del torneo')
@section('page-subtitle-centered', $torneo->nombre)


@section('page-content')

    <div class="row torneos-fields-table">
        <table class="table table-striped table-bordered caption-top">
            <caption>Información del torneo: {{ $torneo->nombre }}</caption>
            <thead>
                <tr>
                    <th scope="col" class="col">Campo</th>
                    <th scope="col" class="col">Valor</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                        <td class="col">ID:</td>
                        <td class="col">{{ $torneo->id }}</td>
                </tr>
                <tr>
                        <td class="col">Nombre:</td>
                        <td class="col">{{ $torneo->nombre }}</td>
                </tr>
                <tr>
                        <td class="col">Deporte:</td>
                        <td class="col">
                            <a class="text-reset text-decoration-none" href="{{ Route('deportes.edit', $torneo->deporte) }}">
                                {{ $torneo->deporte->nombre }}
                            </a>
                        </td>
                </tr>
                <tr>
                        <td class="col">Municipio:</td>
                        <td class="col">
                            <a class="text-reset text-decoration-none" href="{{ Route('municipios.edit', $torneo->municipio) }}">
                                {{ $torneo->municipio->nombre }} {{($torneo->municipio->getDepartamento() !== "") ? '('.$torneo->municipio->getDepartamento().')' : ''}}
                            </a>
                        </td>
                </tr>
                <tr>
                    <td class="col">Escenarios:</td>
                    <td class="col">
                        <ul>
                            @foreach ($torneo->escenarios as $escenario)
                                <li>
                                    <a class="text-reset text-decoration-none" href="{{ Route('escenarios.show', $escenario) }}">
                                        {{ $escenario->nombre }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </td>
                </tr>
                <tr>
                        <td class="col-md-5">Categoria:</td>
                        <td class="col">{{ $torneo->getCategoria() }}</td>
                </tr>
                <tr>
                    <td>Equipos participantes:</td>
                    <td>
                        @if ($torneo->equipos->count() > 0)
                            <ul>
                                @foreach ($torneo->equipos as $equipo)
                                    <li class="row g-5 py-3">
                                        <a href="{{ Route('equipos.show', $equipo) }}" class="text-reset text-decoration-none d-inline col-auto">{{ $equipo->nombre }} ( {{ $equipo->cantidad_integrantes }} participantes)</a>
                                        <form class="d-inline p-0 col-auto" action="{{ Route('torneo.eliminarEquipo', [$torneo, $equipo]) }}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-outline-danger btn-sm">Eliminar</button>
                                        </form>
                                    </li>
                                @endforeach
                            </ul>
                        @else
                            Ninguno
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="row gap-2">
        <form class="d-grid p-0 col" action="{{ Route('torneos.destroy', $torneo) }}" method="post">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-outline-danger">Eliminar</button>
        </form>
        <div class="d-grid p-0 col">
            <a href="{{ Route('torneos.edit', $torneo) }}" class="btn btn-outline-primary">Editar</a>
        </div>
    </div>

    <div class="row mt-5 mb-3"><hr></div>

    <div class="row mb-4">
        <div class="col-12">
            <h5 class="text-center">
                Registrar Equipo en el Torneo
            </h5>
        </div>
    </div>

    @component('components.form')
        @slot('action', Route('torneo.registrarEquipo', $torneo))

        @slot('method', "POST")

        @slot('customAttributes', "class='row g-4'")

        @slot('form_content')

            <div class="col-auto">
                <label for="" class="col-form-label">Equipo:</label>
            </div>

            <div class="col">
                <select class="form-select" id="equipo_id" name="equipo_id" required>
                    <option selected disabled>Seleccionar Equipo</option>
                    @foreach (\App\Models\Equipo::all() as $equipo)
                        @if (!( $torneo->equipos->contains($equipo) ) && $equipo->deporte == $torneo->deporte)
                            <option value="{{ $equipo->id }}">
                                {{ $equipo->nombre }}
                            </option>
                        @endif
                    @endforeach
                </select>
            </div>

            <div class="col">
                <button type="submit" class="btn btn-outline-info w-100">Registrar</button>
            </div>


        @endslot
    @endcomponent

@endsection
