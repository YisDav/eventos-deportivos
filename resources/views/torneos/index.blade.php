@extends('layout.general')

@section('page-title', 'Lista Torneos Registrados')

@section('page-title-centered', 'Lista de torneos registrados')

@section('page-content')

    <div class="row deportes-list-table">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Deporte</th>
                    <th scope="col">Categoria</th>
                    <th scope="col">Municipio</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($torneos as $torneo)
                    <tr>
                        <th scope="row">{{ $torneo->id }}</th>
                        <td>{{ $torneo->nombre }}</td>
                        <td>{{ $torneo->deporte->nombre }} ({{ $torneo->equipos->count() }} equipos)</td>
                        <td>{{ $torneo->getCategoria() }}</td>
                        <td>{{ $torneo->municipio->nombre }} ({{ $torneo->municipio->getDepartamento() }})</td>
                        <td><a href="{{ route('torneos.show', $torneo) }}" class="btn btn-info">Ir</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection
