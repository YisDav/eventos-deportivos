<form action="{{ $action }}" method="{{ $method }}"
    {{-- Atributos que se le quiera añadir al formulario ej: $customAttributes = "style='display:inline;'" --}}
    @isset($customAttributes)
        @php
            echo($customAttributes);
        @endphp
    @endisset
>
    @isset($fake_method)
        @method($fake_method)
    @endisset
    @csrf
    @php
        echo($form_content);
    @endphp
</form>
