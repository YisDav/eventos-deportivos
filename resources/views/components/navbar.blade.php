<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container-fluid">
        <img src="{{ asset('favicon.ico') }}" alt="" class="img-fluid me-3" style="max-width: 35px!important">
        <a class="navbar-brand" href="#">{{ config('app.name') }}</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">Inicio</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Deportes / Equipos / Colegios
                    </a>
                    <ul class="dropdown-menu dropdown-menu-dark">
                        <li class="dropdown-item disabled" aria-disabled="true">Deportes</li>
                        <li><a class="dropdown-item" href="{{ Route('deportes.index') }}">
                            <i class="bi bi-list text-info"></i>
                            Lista
                        </a></li>
                        <li><a class="dropdown-item" href="{{ Route('deportes.create') }}">
                            <i class="bi bi-plus text-danger"></i>
                            Registrar
                        </a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li class="dropdown-item disabled" aria-disabled="true">Equipos</li>
                        <li><a class="dropdown-item" href="{{ Route('equipos.index') }}">
                            <i class="bi bi-list text-info"></i>
                            Lista
                        </a></li>
                        <li><a class="dropdown-item" href="{{ Route('equipos.create') }}">
                            <i class="bi bi-plus text-danger"></i>
                            Registrar
                        </a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li class="dropdown-item disabled" aria-disabled="true">Patrocinadores</li>
                        <li><a class="dropdown-item" href="{{ Route('patrocinadores.index') }}">
                            <i class="bi bi-list text-info"></i>
                            Lista
                        </a></li>
                        <li><a class="dropdown-item" href="{{ Route('patrocinadores.create') }}">
                            <i class="bi bi-plus text-danger"></i>
                            Registrar
                        </a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li class="dropdown-item disabled" aria-disabled="true">Colegios arbitrales</li>
                        <li><a class="dropdown-item" href="{{ Route('colegios.index') }}">
                            <i class="bi bi-list text-info"></i>
                            Lista
                        </a></li>
                        <li><a class="dropdown-item" href="{{ Route('colegios.create') }}">
                            <i class="bi bi-plus text-danger"></i>
                            Registrar
                        </a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Gestion Persona(s)
                    </a>
                    <ul class="dropdown-menu dropdown-menu-dark">
                        <li><a class="dropdown-item" href="{{ Route('personas.index') }}">
                            <i class="bi bi-list text-info"></i>
                            Todas las personas
                        </a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item disabled">Deportista(s)</a></li>
                        <li><a class="dropdown-item" href="{{ Route('deportistas.create') }}">
                            <i class="bi bi-plus text-danger"></i>
                            Registrar
                        </a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item disabled">Entrenadore(s)</a></li>
                        <li><a class="dropdown-item" href="{{ Route('entrenadores.create') }}">
                            <i class="bi bi-plus text-danger"></i>
                            Registrar
                        </a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item disabled">Referi(s)</a></li>
                        <li><a class="dropdown-item" href="{{ Route('referis.create') }}">
                            <i class="bi bi-plus text-danger"></i>
                            Registrar
                        </a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Gestión torneo(s)
                    </a>
                    <ul class="dropdown-menu dropdown-menu-dark">
                        <li class="dropdown-item disabled" aria-disabled="true">Torneos</li>
                        <li><a class="dropdown-item" href="{{ Route('torneos.index') }}">
                            <i class="bi bi-list text-info"></i>
                            Lista
                        </a></li>
                        <li><a class="dropdown-item" href="{{ Route('torneos.create') }}">
                            <i class="bi bi-plus text-danger"></i>
                            Registrar
                        </a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li class="dropdown-item disabled" aria-disabled="true">Escenarios</li>
                        <li><a class="dropdown-item" href="{{ Route('escenarios.index') }}">
                            <i class="bi bi-list text-info"></i>
                            Lista
                        </a></li>
                        <li><a class="dropdown-item" href="{{ Route('escenarios.create') }}">
                            <i class="bi bi-plus text-danger"></i>
                            Registrar
                        </a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li class="dropdown-item disabled" aria-disabled="true">Municipios</li>
                        <li><a class="dropdown-item" href="{{ Route('municipios.index') }}">
                            <i class="bi bi-list text-info"></i>
                            Lista
                        </a></li>
                        <li><a class="dropdown-item" href="{{ Route('municipios.create') }}">
                            <i class="bi bi-plus text-danger"></i>
                            Registrar
                        </a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
