@extends('layout.general')

@section('page-title', "Editar Escenario: $escenario->nombre")


@section('page-title-centered', 'Editar escenario deportivo')
@section('page-subtitle-centered', "$escenario->nombre")


@section('page-content')

    <div class="edit-escenario-form">
        @component('components.form')
            @slot('action', route('escenarios.update', $escenario))

            @slot('method', "POST")

            @slot('fake_method', "PUT")

            @slot('form_content')
                @component('escenarios.components.fields')
                    @slot('escenario', $escenario)
                @endcomponent

                <div class="row">
                    <div class="col d-grid">
                        <button class="btn btn-primary" type="submit">Actualizar</button>
                    </div>
                </div>
            @endslot
        @endcomponent

        <div class="row">
            <div class="col-12 mb-3"></div>
        </div>

        @component('components.form')
            @slot('customAttributes', 'class="d-grid p-0 col"')

            @slot('action', Route('escenarios.destroy', $escenario))

            @slot('method', "POST")

            @slot('fake_method', "DELETE")

            @slot('form_content')
                <button type="submit" class="btn btn-outline-danger">Eliminar</button>
            @endslot
        @endcomponent
    </div>

@endsection
