@php

    $escenarioFix = isset($escenario) ? $escenario : null;

    function getInputvalue($inputName, $escenarioFix) {
        if(old($inputName) !== null) {
            return old($inputName);
        }

        $inputValue = '';
        if($escenarioFix !== null) {
            switch ($inputName) {
                case 'nombre':
                    $inputValue = $escenarioFix->nombre;
                    break;
                case 'capacidad':
                    $inputValue = $escenarioFix->capacidad;
                    break;
                case 'municipio':
                    $inputValue = $escenarioFix->municipio->nombre;
                    break;
                case 'ids_deportes':
                    $inputValue = $escenarioFix->deportes;
                    break;
            }
        }
        else {
            switch ($inputName) {
                case 'capacidad':
                    $inputValue = "1";
                    break;
            }
        }
        return $inputValue;
    }

@endphp

<div class="row g-2 mb-2">
    <div class="col">
        <div class="form-floating">
            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre del escenario" value="{{ getInputvalue('nombre', $escenarioFix) }}" required>
            <label for="nombre">Nombre del escenario:</label>
        </div>
    </div>
</div>
<div class="row g-2 mb-2">
    <div class="col-md">
        <div class="form-floating">
            <input type="number" class="form-control" id="capacidad" name="capacidad" min="-11111111" value="{{ getInputvalue('capacidad', $escenarioFix) }}" required>
            <label for="capacidad">Capacidad:</label>
        </div>
    </div>
</div>
<div class="row g-2 mb-2">
    <div class="col">
        <div class="form-floating">
            <input class="form-control" list="datalistOptions" id="municipio_nombre" name="municipio_nombre" value="{{ getInputValue('municipio', $escenarioFix) }}">
            <datalist id="datalistOptions">
                @foreach (\App\Models\Municipio::all() as $municipio)
                    <option value="{{ $municipio->nombre }}">
                        {{ $municipio->nombre }} {{ ($municipio->getDepartamento() !== "") ? '('.$municipio->getDepartamento().')' : '' }}
                    </option>
                @endforeach
            </datalist>
            <label for="datalistOptions" class="form-label">Municipio</label>
        </div>
    </div>
</div>
<div class="row g-2 mb-2">
    <div class="col">
        <label for="ids_deportes" class="form-label">Deportes practicados (usar CTRL para seleccionar más de uno):</label>
        <select class="form-select" multiple aria-label="multiple select deportes" id="ids_deportes" name="ids_deportes[]" multiple required>
            <option disabled>Seleccionar deportes</option>
            @foreach (App\Models\Deporte::all() as $deporte)
                <option
                    @if (getInputvalue('ids_deportes', $escenarioFix) !== '')
                        @if (is_array(getInputvalue('ids_deportes', $escenarioFix)))
                            @if (in_array($deporte->id, getInputvalue('ids_deportes', $escenarioFix)))
                                selected
                            @endif
                        @else
                            @if (getInputvalue('ids_deportes', $escenarioFix)->contains($deporte->id))
                                selected
                            @endif
                        @endif
                    @endif
                value="{{ $deporte->id }}">
                    {{ $deporte->nombre }}
                </option>
            @endforeach
        </select>
    </div>
</div>
