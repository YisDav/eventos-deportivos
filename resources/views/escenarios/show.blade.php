@extends('layout.general')

@section('page-title', "Escenario: $escenario->nombre")


@section('page-title-centered', 'Información del escenario deportivo')
@section('page-subtitle-centered', $escenario->nombre)


@section('page-content')

    <div class="row escenarios-fields-table">
        <table class="table table-striped table-bordered caption-top">
            <caption>Información del escenario: {{ $escenario->nombre }}</caption>
            <thead>
                <tr>
                    <th scope="col" class="col">Campo</th>
                    <th scope="col" class="col">Valor</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                        <td class="col">ID:</td>
                        <td class="col">{{ $escenario->id }}</td>
                </tr>
                <tr>
                        <td class="col">Nombre:</td>
                        <td class="col">{{ $escenario->nombre }}</td>
                </tr>
                <tr>
                        <td class="col">Municipio:</td>
                        <td class="col">
                            <a class="text-reset text-decoration-none" href="{{ Route('municipios.edit', $escenario->municipio) }}">
                                {{ $escenario->municipio->nombre }}
                            </a>
                        </td>
                </tr>
                <tr>
                        <td class="col">Capacidad:</td>
                        <td class="col">{{ $escenario->capacidad }}</td>
                </tr>
                <tr>
                        <td class="col">N. Deportes Registrados:</td>
                        <td class="col">{{ $escenario->deportes->count() }}</td>
                </tr>
                <tr>
                        <td class="col">Deportes Registrados:</td>
                        <td class="col">
                            <ul>
                                @foreach ($escenario->deportes as $deporte)
                                    <li>
                                        <a class="text-reset text-decoration-none" href="{{ Route('deportes.edit', $deporte) }}">
                                            {{ $deporte->nombre }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="row gap-2">
        <form class="d-grid p-0 col" action="{{ Route('escenarios.destroy', $escenario) }}" method="post">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-outline-danger">Eliminar</button>
        </form>
        <div class="d-grid p-0 col">
            <a href="{{ Route('escenarios.edit', $escenario) }}" class="btn btn-outline-primary">Editar</a>
        </div>
    </div>

@endsection
