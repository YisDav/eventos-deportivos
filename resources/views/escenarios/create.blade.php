@extends('layout.general')
@section('page-title', "Registrar Escenario Deportivo")

@section('page-title-centered', 'Registrar un nuevo escenario deportivo')

@section('page-content')

    <div class="create-deportista-form">
        @component('components.form')
            @slot('action', route('escenarios.store'))

            @slot('method', "POST")

            @slot('form_content')
                @component('escenarios.components.fields')
                @endcomponent

                <div class="row">
                    <div class="col d-grid">
                        <button class="btn btn-primary" type="submit">Registrar</button>
                    </div>
                </div>
            @endslot
        @endcomponent
    </div>

@endsection
