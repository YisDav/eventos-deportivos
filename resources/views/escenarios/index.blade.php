@extends('layout.general')

@section('page-title', 'Lista Escenarios Deportivos Registrados')

@section('page-title-centered', 'Lista de escenarios deportivos registrados')

@section('page-content')

    <div class="row deportes-list-table">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Capacidad</th>
                    <th scope="col"># Deportes</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($escenarios as $escenario)
                    <tr>
                        <th scope="row">{{ $escenario->id }}</th>
                        <td>{{ $escenario->nombre }}</td>
                        <td>{{ $escenario->capacidad }}</td>
                        <td>{{ $escenario->deportes->count() }}</td>
                        <td><a href="{{ route('escenarios.show', $escenario) }}" class="btn btn-info">Ir</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection
