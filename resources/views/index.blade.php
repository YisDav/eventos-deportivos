@extends('layout.general')

@section('page-title', 'Inicio')
@section('page-container-class', 'container-fluid')

@section('page_upper_content')

    <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
        <div class="col-md-5 p-lg-5 mx-auto my-5">
            <h1 class="display-6 fw-normal">Eventos Deportivos</h1>
            <p class="lead fw-normal">Sistema Integral de gestión de Torneos Deportivos.</p>
            <br>
            <a class="btn btn-outline-primary" href="{{ Route('torneos.index') }}">Ver Torneos Activos</a>
        </div>
        <div class="product-device shadow-sm d-none d-md-block"></div>
        <div class="product-device product-device-2 shadow-sm d-none d-md-block"></div>
    </div>

@endsection
