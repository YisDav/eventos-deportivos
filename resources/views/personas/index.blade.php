@extends('layout.general')

@section('page-title', 'Lista Personas')


@section('page-title-centered', 'Lista de personas registradas')


@section('page-content')
    <div class="row personas-list-table">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Cédula</th>
                    <th scope="col">Nombre Completo</th>
                    <th scope="col">Rol</th>
                    <th scope="col">Ir</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($personas as $persona)
                    <tr>
                        <th scope="row">{{ $persona->id }}</th>
                        <td>{{ $persona->identificacion }}</td>
                        <td>{{ $persona->nombre }} {{ $persona->apellido }}</td>
                        <td>{{ Str::ucfirst($persona->getRol()) }}</td>
                        <td><a href="{{ route('personas.show', $persona->id) }}" class="btn btn-info">ir</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection
