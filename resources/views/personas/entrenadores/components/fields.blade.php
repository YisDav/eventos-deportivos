@php
    $personaFix = isset($entrenador) ? $entrenador->participante->persona : null;
@endphp

@component('personas.components.participantes_fields')
    @isset($entrenador)
        @slot('persona', $entrenador->participante->persona)
    @endisset
@endcomponent

<div class="row g-2 mb-2">
    <div class="col-md">
        <div class="form-floating">
            <input type="number" class="form-control" id="anios_experiencia" name="anios_experiencia" min="0" value="{{ getInputvalue('anios_experiencia', $personaFix) }}" required>
            <label for="anios_experiencia">Años de experiencia:</label>
        </div>
    </div>
</div>
