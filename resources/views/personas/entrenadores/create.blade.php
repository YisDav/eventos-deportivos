@extends('layout.general')
@section('page-title', "Registrar Entrenador")

@section('page-title-centered', 'Registrar nuevo entrenador')

@section('page-content')

    <div class="create-deportista-form">
        @component('components.form')
            @slot('action', route('entrenadores.store'))

            @slot('method', "POST")

            @slot('form_content')
                @component('personas.components.fields')
                @endcomponent

                @component('personas.entrenadores.components.fields')
                @endcomponent

                <div class="row">
                    <div class="col d-grid">
                        <button class="btn btn-primary" type="submit">Registrar</button>
                    </div>
                </div>
            @endslot
        @endcomponent
    </div>

@endsection
