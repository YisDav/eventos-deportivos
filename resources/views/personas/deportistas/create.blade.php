@extends('layout.general')
@section('page-title', "Registrar Deportista")

@section('page-title-centered', 'Registrar nuevo deportista')

@section('page-content')

    <div class="create-deportista-form">
        @component('components.form')
            @slot('action', route('deportistas.store'))

            @slot('method', "POST")

            @slot('form_content')
                @component('personas.components.fields')
                @endcomponent

                @component('personas.deportistas.components.fields')
                @endcomponent

                <div class="row">
                    <div class="col d-grid">
                        <button class="btn btn-primary" type="submit">Registrar</button>
                    </div>
                </div>
            @endslot
        @endcomponent
    </div>

@endsection
