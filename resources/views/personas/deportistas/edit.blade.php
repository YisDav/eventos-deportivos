@php
    $persona = $deportista->participante->persona;
@endphp

@extends('layout.general')

@section('page-title', "Editar Registro: $persona->nombre $persona->apellido")


@section('page-title-centered', 'Editar registro')
@section('page-subtitle-centered', "$persona->nombre $persona->apellido")


@section('page-content')

    <div class="edit-deportista-form">
        @component('components.form')
            @slot('action', route('deportistas.update', $deportista))

            @slot('method', "POST")

            @slot('fake_method', "PUT")

            @slot('form_content')
                @component('personas.components.fields')
                    @slot('persona', $persona)
                @endcomponent

                @component('personas.deportistas.components.fields')
                    @slot('deportista', $deportista)
                @endcomponent

                <div class="row">
                    <div class="col d-grid">
                        <button class="btn btn-primary" type="submit">Actualizar</button>
                    </div>
                </div>
            @endslot
        @endcomponent
    </div>

@endsection
