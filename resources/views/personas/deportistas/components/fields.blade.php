@php
    $personaFix = isset($deportista) ? $deportista->participante->persona : null;
@endphp

@component('personas.components.participantes_fields')
    @isset($deportista)
        @slot('persona', $personaFix)
    @endisset
@endcomponent

<div class="row g-2 mb-2">
    <div class="col-md">
        <div class="form-floating">
            <input type="number" class="form-control" id="rendimiento" name="rendimiento" value="{{ getInputvalue('rendimiento', $personaFix) }}" required>
            <label for="rendimiento">Indice de rendimiento:</label>
        </div>
    </div>
</div>
