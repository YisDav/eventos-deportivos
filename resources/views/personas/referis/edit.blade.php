@php
    $persona = $referi->persona;
@endphp

@extends('layout.general')

@section('page-title', "Editar Registro: $persona->nombre $persona->apellido")


@section('page-title-centered', 'Editar registro')
@section('page-subtitle-centered', "$persona->nombre $persona->apellido")


@section('page-content')

    <div class="edit-referi-form">
        @component('components.form')
            @slot('action', route('referis.update', $referi))

            @slot('method', "POST")

            @slot('fake_method', "PUT")

            @slot('form_content')
                @component('personas.components.fields')
                    @slot('persona', $persona)
                @endcomponent

                @component('personas.referis.components.fields')
                    @slot('persona', $persona)
                @endcomponent

                <div class="row">
                    <div class="col d-grid">
                        <button class="btn btn-primary" type="submit">Actualizar</button>
                    </div>
                </div>
            @endslot
        @endcomponent
    </div>

@endsection
