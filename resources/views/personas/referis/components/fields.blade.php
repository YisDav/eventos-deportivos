@php
    $personaFix = isset($persona) ? $persona : null;
@endphp

<div class="row g-2 mb-2">
    <div class="col-md">
        <div class="form-floating">
            <select class="form-select" id="colegio_id" name="colegio_id" required>
                <option disabled>Seleccionar Colegio</option>
                @foreach (\App\Models\Colegio::all() as $colegio)
                    <option
                        value="{{ $colegio->id }}"
                        @if (getInputvalue('colegio_id', $personaFix) == $colegio->id)
                            selected
                        @endif
                    >
                        {{ $colegio->nombre }}
                    </option>
                @endforeach
            </select>
            <label for="colegio_id">Colegio al que pertenece:</label>
        </div>
    </div>
</div>
<div class="row g-2 mb-2">
    <div class="col-md">
        <div class="form-floating">
            <input type="number" class="form-control" id="juegos_reglamentados" name="juegos_reglamentados" min="0" value="{{ getInputvalue('juegos_reglamentados', $personaFix) }}" required>
            <label for="juegos_reglamentados">Juegos reglamentados:</label>
        </div>
    </div>
</div>
