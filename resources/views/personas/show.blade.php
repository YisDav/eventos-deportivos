@extends('layout.general')

@section('page-title', "Persona: $persona->nombre $persona->apellido")


@section('page-title-centered', 'Información personal')
@section('page-subtitle-centered', "$persona->nombre $persona->apellido")


@section('page-content')
    <div class="row personas-fields-table">
        <table class="table table-striped table-bordered caption-top">
            <caption>Información del {{strtolower($persona->getRol())}}: {{ $persona->nombre }} {{ $persona->apellido }}</caption>
            <thead>
                <tr>
                    <th scope="col" class="col-6">Campo</th>
                    <th scope="col" class="col-6">Valor</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                        <td class="col">ID:</td>
                        <td class="col">{{ $persona->id }}</td>
                </tr>
                <tr>
                        <td class="col">Identificación:</td>
                        <td class="col">{{ $persona->identificacion }}</td>
                </tr>
                <tr>
                        <td class="col">Nombre:</td>
                        <td class="col">{{ $persona->nombre }}</td>
                </tr>
                <tr>
                        <td class="col">Apellido:</td>
                        <td class="col">{{ $persona->apellido }}</td>
                </tr>
                <tr>
                        <td class="col">Telefono:</td>
                        <td class="col">{{ $persona->telefono }}</td>
                </tr>
                <tr>
                    <td class="col">Tipo de participante / Rol:</td>
                    <td class="col">{{ Str::ucfirst($persona->getRol()) }}</td>

                </tr>

                @if ($persona->getRol() == 'Deportista' || $persona->getRol() == 'Entrenador')
                    <tr>
                        <td class="col">Equipo:</td>
                        <td class="col">
                            <a class="text-reset text-decoration-none" href="{{ Route('equipos.show', $persona->participante->equipo) }}">
                                {{ $persona->participante->equipo->nombre }} ({{ $persona->participante->equipo->deporte->nombre }})
                            </a>
                        </td>
                    </tr>
                    @if ($persona->getRol() == 'Deportista')
                        <tr>
                            <td class="col">Indice de rendimiento:</td>
                            <td class="col">{{ $persona->participante->participa_en->rendimiento }}</td>
                        </tr>
                    @elseif ($persona->getRol() == 'Entrenador')
                        <tr>
                            <td class="col">Años de experiencia:</td>
                            <td class="col">{{ $persona->participante->participa_en->anios_experiencia }}</td>
                        </tr>
                    @endif
                @elseif ($persona->getRol() == 'Réferi')
                    <tr>
                        <td class="col">Colegio Arbitral:</td>
                        <td class="col">
                            <a class="text-reset text-decoration-none" href="{{ Route('colegios.edit', $persona->referi->colegio) }}">
                                {{ $persona->referi->colegio->nombre }}
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td class="col">Juegos Reglamentados:</td>
                        <td class="col">{{ $persona->referi->juegos_reglamentados }}</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
    <div class="row gap-2">
        <form class="d-grid p-0 col" action="{{ Route('personas.destroy', $persona) }}" method="post">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-outline-danger">Eliminar</button>
        </form>
        <div class="d-grid p-0 col">
            <a
                @if ($persona->getRol() == 'Entrenador')
                    href="{{ Route('entrenadores.edit', $persona->participante->participa_en) }}"
                @elseif ($persona->getRol() == 'Deportista')
                    href="{{ Route('deportistas.edit', $persona->participante->participa_en) }}"
                @elseif ($persona->getRol() == 'Réferi')
                    href="{{ Route('referis.edit', $persona->referi) }}"
                @endif
            class="btn btn-outline-primary">
                Editar
            </a>
        </div>
    </div>

@endsection
