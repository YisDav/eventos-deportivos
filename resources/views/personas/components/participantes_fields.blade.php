@php
    $personaFix = isset($persona) ? $persona : null;
@endphp
<div class="row g-2 mb-2">
    <div class="col-md">
        <div class="form-floating">
            <select class="form-select" id="equipo_id" name="equipo_id" required>
                <option disabled>Seleccionar Equipo</option>
                @foreach (\App\Models\Equipo::all() as $equipo)
                    <option
                        value="{{ $equipo->id }}"
                        @if (getInputvalue('equipo_id', $personaFix) == $equipo->id)
                            selected
                        @endif
                    >
                        {{ $equipo->nombre }}
                    </option>
                @endforeach
            </select>
            <label for="equipo_id">Equipo al que pertenece:</label>
        </div>
    </div>
</div>
