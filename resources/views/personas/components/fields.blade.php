@php

    $personaFix = isset($persona) ? $persona : null;

    function getInputvalue($inputName, $personaFixF) {
        if(old($inputName) !== null) {
            return old($inputName);
        }

        $inputValue = '';
        if($personaFixF !== null) {
            switch ($inputName) {
                case 'identificacion':
                    $inputValue = $personaFixF->identificacion;
                    break;
                case 'nombre':
                    $inputValue = $personaFixF->nombre;
                    break;
                case 'apellido':
                    $inputValue = $personaFixF->apellido;
                    break;
                case 'direccion':
                    $inputValue = $personaFixF->direccion;
                    break;
                case 'telefono':
                    $inputValue = $personaFixF->telefono;
                    break;
                case 'equipo_id':
                    $inputValue = $personaFixF->participante->equipo_id;
                    break;
                case 'rendimiento':
                    $inputValue = $personaFixF->participante->participa_en->rendimiento;
                    break;
                case 'anios_experiencia':
                    $inputValue = $personaFixF->participante->participa_en->anios_experiencia;
                    break;
                case 'colegio_id':
                    $inputValue = $personaFixF->referi->colegio_id;
                    break;
                case 'juegos_reglamentados':
                    $inputValue = $personaFixF->referi->juegos_reglamentados;
                    break;
            }
        }

        else {
            switch ($inputName) {
                case 'rendimiento':
                    $inputValue = 100;
                    break;

                case 'anios_experiencia':
                    $inputValue = 0;
                    break;
            }
        }

        return $inputValue;
    }

@endphp

<div class="row g-2 mb-2">
    <div class="col">
        <div class="form-floating">
            <input type="number" class="form-control" id="identificacion" name="identificacion" value="{{ getInputvalue('identificacion', $personaFix) }}" required>
            <label for="identificacion">Identificación:</label>
        </div>
    </div>
</div>
<div class="row g-2 mb-2">
    <div class="col-md">
        <div class="form-floating">
            <input type="text" class="form-control" id="nombre" name="nombre" value="{{ getInputvalue('nombre', $personaFix) }}" required>
            <label for="nombre">Nombre:</label>
        </div>
    </div>
    <div class="col-md">
        <div class="form-floating">
            <input type="text" class="form-control" id="apellido" name="apellido" value="{{ getInputvalue('apellido', $personaFix) }}" required>
            <label for="apellido">Apellido:</label>
        </div>
    </div>
</div>
<div class="row g-2 mb-2">
    <div class="col-md">
        <div class="form-floating">
            <input type="text" class="form-control" id="direccion" name="direccion" value="{{ getInputvalue('direccion', $personaFix) }}" required>
            <label for="direccion">Dirección:</label>
        </div>
    </div>
    <div class="col-md">
        <div class="form-floating">
            <input type="number" class="form-control" id="telefono" name="telefono" value="{{ getInputvalue('telefono', $personaFix) }}" required>
            <label for="telefono">Teléfono:</label>
        </div>
    </div>
</div>
