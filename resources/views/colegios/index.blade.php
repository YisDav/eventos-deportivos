@extends('layout.general')

@section('page-title', 'Lista Colegios Registrados')

@section('page-title-centered', 'Lista de colegios registrados')

@section('page-content')

    <div class="row colegios-list-table">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($colegios as $colegio)
                    <tr>
                        <th scope="row">{{ $colegio->id }}</th>
                        <td class="col-md-6">{{ $colegio->nombre }}</td>
                        <td class="col-md-3"><a href="{{ route('colegios.edit', $colegio) }}" class="btn btn-info">Editar</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection
