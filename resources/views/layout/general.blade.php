<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <base href="{{ env('APP_URL') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">

    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">

    <!-- Other CSS -->
    @yield("page-styles", "")

    <title>@yield("page-title", "General") | {{ config('app.name') }}</title>
</head>
<body>
    <div class="navbar-component">
        @component("components.navbar")
        @endcomponent
    </div>

    <div class="@yield("page-container-class", "container") mb-5" style="min-height: 80vh;"> {{-- Select container type, either container-fluid or container [Bootstrap] --}}
        @yield('page_upper_content', '')
        <div class="row"><div class="col-12 mb-5"></div></div>
        <h5 class="text-center">
            @yield("page-title-centered", '')
            <br>
            <small class="text-muted">
                @yield("page-subtitle-centered", '')
            </small>
        </h5>
        <div class="row"><div class="col-12 mb-4"></div></div>
        <div class="row">
            <div class="col-12 messages">
                @if ($errors->any())
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
            </div>
        </div>
        @yield("page-content", "")
    </div>

    <div class="container-fluid">
        <div class="row">]
            <footer class="col-12 bg-dark py-2">
                <p class="small text-center text-light mt-2">
                    Sitio creado por: <a class="text-white text-decoration-none" href="https://co.linkedin.com/in/yisdav">Jesús D. García Cuello</a>.
                    <br>
                    Como actividad/evidencia para el Servicio Nacional de Aprendizaje SENA © 2022
                </p>
            </footer>
        </div>
    </div>

    <div class="scripts" style="display:none;">
        <!-- Bootstrap -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>

        <!-- Other JS -->
        @yield("page-scripts", "")
    </div>
</body>
</html>
