@extends('layout.general')

@section('page-title', 'Editar Deporte: {{ $deporte->nombre }}')


@section('page-title-centered', 'Editar deporte')
@section('page-subtitle-centered', $deporte->nombre)


@section('page-content')

    <div class="editar-deporte-form">
        <form action="{{ Route("deportes.update", $deporte) }}" method="post">
            @method("PUT")
            @csrf
            <div class="row g-2 mb-2">
                <div class="col">
                    <div class="form-floating">
                        <input type="text" class="form-control" id="staticID" value="{{ $deporte->id }}" readonly>
                        <label for="nombre">ID:</label>
                    </div>
                </div>
            </div>
            <div class="row g-2 mb-2">
                <div class="col">
                    <div class="form-floating">
                        <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $deporte->nombre }}" required>
                        <label for="nombre">Nombre:</label>
                    </div>
                </div>
            </div>
            <div class="row g-2 mb-2">
                <div class="col">
                    <div class="form-floating">
                        <textarea class="form-control" id="descripcion" name="descripcion" style="height: 100px">{{ $deporte->descripcion }}</textarea>
                        <label for="descripcion">Descripción (opcional):</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col d-grid">
                    <button class="btn btn-primary" type="submit">Actualizar</button>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-12 mb-3"></div>
        </div>
        <form class="d-grid p-0 col" action="{{ Route('deportes.destroy', $deporte) }}" method="post">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-outline-danger">Eliminar</button>
        </form>
    </div>

@endsection
