@extends('layout.general')

@section('page-title', 'Nuevo Deporte')

@section('page-title-centered', 'Registrar nuevo deporte')

@section('page-content')
    <div class="create-deportes-form">
        <form action="{{ Route("deportes.store") }}" method="post">
            @csrf
            <div class="row g-2 mb-2">
                <div class="col">
                    <div class="form-floating">
                        <input type="text" class="form-control" id="nombre" name="nombre" required>
                        <label for="nombre">Nombre:</label>
                    </div>
                </div>
            </div>
            <div class="row g-2 mb-2">
                <div class="col">
                    <div class="form-floating">
                        <textarea class="form-control" id="descripcion" name="descripcion" style="height: 100px"></textarea>
                        <label for="descripcion">Descripción (opcional):</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col d-grid">
                    <button class="btn btn-primary" type="submit">Registrar</button>
                </div>
            </div>
        </form>
    </div>

@endsection
