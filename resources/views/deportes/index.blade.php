@extends('layout.general')

@section('page-title', 'Lista Deportes Registrados')

@section('page-title-centered', 'Lista de deportes registrados')

@section('page-content')

    <div class="row deportes-list-table">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">N. Equipos</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($deportes as $deporte)
                    <tr>
                        <th scope="row">{{ $deporte->id }}</th>
                        <td>{{ $deporte->nombre }}</td>
                        <td>{{ $deporte->equipos->count() }} ({{ $deporte->numero_participantes() }})</td>
                        <td><a href="{{ route('deportes.edit', $deporte) }}" class="btn btn-info">Editar</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection
