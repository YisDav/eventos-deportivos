@extends('layout.general')

@section('page-title', 'Lista Equipos Registrados')

@section('page-title-centered', 'Lista de equipos registados')

@section('page-content')

    <div class="row equipos-list-table">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Deporte</th>
                    <th scope="col">Integrantes</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($equipos as $equipo)
                    <tr>
                        <th scope="row">{{ $equipo->id }}</th>
                        <td>{{ $equipo->nombre }}</td>
                        <td>{{ $equipo->deporte->nombre }}</td>
                        <td>{{ $equipo->cantidad_integrantes }}</td>
                        <td><a href="{{ route('equipos.show', $equipo) }}" class="btn btn-info">Ir</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection
