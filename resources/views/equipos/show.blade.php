@extends('layout.general')

@section('page-title', "Equipo: $equipo->nombre")


@section('page-title-centered', 'Datos del equipo')
@section('page-subtitle-centered', $equipo->nombre)


@section('page-content')

    <div class="row equipos-fields-table">
        <table class="table table-striped table-bordered caption-top">
            <caption>Información de {{ $equipo->nombre }}</caption>
            <thead>
                <tr>
                    <th scope="col" class="col">Campo</th>
                    <th scope="col" class="col">Valor</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                        <td class="col">ID:</td>
                        <td class="col">{{ $equipo->id }}</td>
                </tr>
                <tr>
                        <td class="col">Nombre:</td>
                        <td class="col">{{ $equipo->nombre }}</td>
                </tr>
                <tr>
                        <td class="col">Deporte:</td>
                        <td class="col">{{ $equipo->deporte->nombre }} ({{ $equipo->deporte->id }})</td>
                </tr>
                <tr>
                        <td class="col">N. Integrantes:</td>
                        <td class="col">{{ $equipo->cantidad_integrantes }}</td>
                </tr>
                @if ($equipo->cantidad_integrantes > 0)
                    <tr>
                        <td class="col">Participantes</td>
                        <td class="col-6">
                            <ul class="list-group list-group-flush">
                                @foreach ($equipo->participantes as $participante)
                                    <li class="list-group-item" style="background-color: #fff0!important;">
                                        <a class="text-reset text-decoration-none" href="{{ Route('personas.show', $participante->persona) }}">{{ $participante->persona->nombre }} {{ $participante->persona->apellido }} ({{ $participante->rol() }})</a>
                                    </li>
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
    <div class="row gap-2">
        <form class="d-grid p-0 col" action="{{ Route('equipos.destroy', $equipo) }}" method="post">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-outline-danger">Eliminar</button>
        </form>
        <div class="d-grid p-0 col">
            <a href="{{ Route('equipos.edit', $equipo) }}" class="btn btn-outline-primary">Editar</a>
        </div>
    </div>

@endsection
