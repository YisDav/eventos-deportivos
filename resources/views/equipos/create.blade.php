@extends('layout.general')

@section('page-title', 'Nuevo Equipo')

@section('page-title-centered', 'Registrar nuevo equipo')

@section('page-content')

    <div class="editar-equipos-form">
        <form action="{{ Route("equipos.store") }}" method="post">
            @csrf
            <div class="row g-2 mb-2">
                <div class="col">
                    <div class="form-floating">
                        <input type="text" class="form-control" id="nombre" name="nombre" required>
                        <label for="nombre">Nombre del equipo:</label>
                    </div>
                </div>
            </div>
            <div class="row g-2 mb-2">
                <div class="col">
                    <div class="form-floating">
                        <select class="form-select" id="deporte_id" name="deporte_id" required>
                            <option disabled>Seleccionar deporte</option>
                            @foreach ($deportes as $deporte)
                                <option value="{{ $deporte->id }}">{{ $deporte->nombre }}</option>
                            @endforeach
                        </select>
                        <label for="descripcion">Deporte que practica:</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col d-grid">
                    <button class="btn btn-primary" type="submit">Registrar</button>
                </div>
            </div>
        </form>
    </div>

@endsection
