@extends('layout.general')

@section('page-title', "Editar Equipo: $equipo->nombre")


@section('page-title-centered', 'Editar equipo')
@section('page-subtitle-centered', $equipo->nombre)


@section('page-content')

    <div class="editar-equipos-form">
        <form action="{{ Route("equipos.update", $equipo) }}" method="post">
            @method("PUT")
            @csrf
            <div class="row g-2 mb-2">
                <div class="col">
                    <div class="form-floating">
                        <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $equipo->nombre }}" required>
                        <label for="nombre">Nombre del equipo:</label>
                    </div>
                </div>
            </div>
            <div class="row g-2 mb-2">
                <div class="col">
                    <div class="form-floating">
                        <select class="form-select" id="deporte_id" name="deporte_id" required>
                            <option disabled>Seleccionar deporte</option>
                            @foreach ($deportes as $deporte)
                                <option
                                    value="{{ $deporte->id }}"
                                    @if ($equipo->deporte->id == $deporte->id)
                                        selected
                                    @endif
                                >
                                        {{ $deporte->nombre }}
                                </option>
                            @endforeach
                        </select>
                        <label for="descripcion">Deporte que practica:</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col d-grid">
                    <button class="btn btn-primary" type="submit">Actualizar</button>
                </div>
            </div>
        </form>
        <div class="col-12 mb-3"></div>
        <form class="d-grid p-0 col" action="{{ Route('equipos.destroy', $equipo) }}" method="post">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-outline-danger">Eliminar</button>
        </form>
    </div>

@endsection
