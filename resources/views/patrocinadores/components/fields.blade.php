@php

    $patrocinadorFix = isset($patrocinador) ? $patrocinador : null;

    function getInputvalue($inputName, $patrocinadorFix) {
        if(old($inputName) !== null) {
            return old($inputName);
        }

        $inputValue = '';
        if($patrocinadorFix !== null) {
            return $patrocinadorFix[$inputName];
        }
    }

@endphp

<div class="row g-2 mb-2">
    <div class="col">
        <div class="form-floating">
            <input type="string" class="form-control" id="identificacion" name="identificacion" placeholder="Identificacion del patrocinador" value="{{ getInputvalue('identificacion', $patrocinadorFix) }}" required>
            <label for="identificacion">Identificación / NIT:</label>
        </div>
    </div>
</div>

<div class="row g-2 mb-2">
    <div class="col">
        <div class="form-floating">
            <input type="text" class="form-control" id="razon_nombre" name="razon_nombre" placeholder="Nombre del patrocinador" value="{{ getInputvalue('razon_nombre', $patrocinadorFix) }}" required>
            <label for="razon_nombre">Nombre / Razón Social:</label>
        </div>
    </div>
</div>
