@extends('layout.general')

@section('page-title', 'Lista Patrocinadores Registrados')

@section('page-title-centered', 'Lista de patrocinadores registrados')

@section('page-content')

    <div class="row deportes-list-table">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Identificación</th>
                    <th scope="col">Nombre / Razón social</th>
                    <th scope="col">No. Equipos Patrocinados</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($patrocinadores as $patrocinador)
                    <tr>
                        <th scope="row"></th>
                        <td>{{ $patrocinador->identificacion }}</td>
                        <td>{{ $patrocinador->razon_nombre }}</td>
                        @if ($patrocinador->patrocinios->count() !== 0)
                            <td>{{ $patrocinador->patrocinios->count() }}</td>
                        @else
                            <td>Ninguno</td>
                        @endif
                        <td><a href="{{ route('patrocinadores.show', $patrocinador) }}" class="btn btn-info">Ir</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection
