@extends('layout.general')

@section('page-title', "Patrocinador: $patrocinador->razon_nombre")


@section('page-title-centered', 'Información del patrocinador')
@section('page-subtitle-centered', $patrocinador->razon_nombre)


@section('page-content')

    <div class="row patrocinadors-fields-table">
        <table class="table table-striped table-bordered caption-top">
            <caption>Información del patrocinador: {{ $patrocinador->razon_nombre }}</caption>
            <thead>
                <tr>
                    <th scope="col" class="col">Campo</th>
                    <th scope="col" class="col">Valor</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                        <td class="col-md-6">ID:</td>
                        <td class="col">{{ $patrocinador->id }}</td>
                </tr>
                <tr>
                        <td class="col">Nombre / Razón Social:</td>
                        <td class="col">{{ $patrocinador->razon_nombre }}</td>
                </tr>
                <tr>
                    <td class="col">Equipos patrocinados ({{ $patrocinador->patrocinios->count() }}):</td>
                    <td class="col">
                        @if ( $patrocinador->patrocinios->count() !== 0 )
                            <ul>
                                @foreach ($patrocinador->patrocinios as $patrocinio)
                                    <li class="row g-5 py-3">
                                        <span class="d-inline col-auto">{{ $patrocinio->nombre }} (Monto: {{ $patrocinio->pivot->monto }})</span>
                                        <form class="d-inline p-0 col-auto" action="{{ Route('patrocinadores.unsponsor', [$patrocinador, $patrocinio]) }}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-outline-danger btn-sm">Eliminar</button>
                                        </form>
                                    </li>
                                @endforeach
                            </ul>
                        @else
                            Ninguno
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="row gap-2">
        <form class="d-grid p-0 col" action="{{ Route('patrocinadores.destroy', $patrocinador) }}" method="post">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-outline-danger">Eliminar</button>
        </form>
        <div class="d-grid p-0 col">
            <a href="{{ Route('patrocinadores.edit', $patrocinador) }}" class="btn btn-outline-primary">Editar</a>
        </div>
    </div>

    <div class="row mt-5 mb-3"><hr></div>

    <div class="row mb-4">
        <div class="col-12">
            <h5 class="text-center">
                Nuevo Patrocinio
            </h5>
        </div>
    </div>

    @component('components.form')
        @slot('action', Route('patrocinadores.sponsor', $patrocinador))

        @slot('method', "POST")

        @slot('customAttributes', "class='row g-3'")

        @slot('form_content')

            <div class="col-auto">
                <label for="" class="col-form-label">Equipo:</label>
            </div>

            <div class="col">
                <select class="form-select" id="equipo_id" name="equipo_id" required>
                    <option selected disabled>Seleccionar Equipo</option>
                    @foreach (\App\Models\Equipo::all() as $equipo)
                        <option value="{{ $equipo->id }}">
                            {{ $equipo->nombre }}
                        </option>
                    @endforeach
                </select>
            </div>


            <div class="col-auto">
                <label for="monto" class="col-form-label">Monto:</label>
            </div>

            <div class="col input-group mb-3">
                <span class="input-group-text">$</span>
                <input type="number" class="form-control" id="monto" name="monto" value="500000" min="500000">
                <span class="input-group-text">.00</span>
            </div>


            <div class="col-auto">
                <button type="submit" class="btn btn-info">Guardar</button>
            </div>


        @endslot
    @endcomponent

@endsection
