@extends('layout.general')
@section('page-title', "Registrar Patrocinador")

@section('page-title-centered', 'Registrar Nuevo Patrocinador')

@section('page-content')

    <div class="create-deportista-form">
        @component('components.form')
            @slot('action', route('patrocinadores.store'))

            @slot('method', "POST")

            @slot('form_content')
                @component('patrocinadores.components.fields')
                @endcomponent

                <div class="row">
                    <div class="col d-grid">
                        <button class="btn btn-primary" type="submit">Registrar</button>
                    </div>
                </div>
            @endslot
        @endcomponent
    </div>

@endsection
