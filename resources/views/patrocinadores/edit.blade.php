@extends('layout.general')

@section('page-title', "Editar Patrocinador: $patrocinador->razon_nombre")


@section('page-title-centered', 'Editar Patrocinador')
@section('page-subtitle-centered', "$patrocinador->razon_nombre")


@section('page-content')

    <div class="edit-patrocinador-form">
        @component('components.form')
            @slot('action', route('patrocinadores.update', $patrocinador))

            @slot('method', "POST")

            @slot('fake_method', "PUT")

            @slot('form_content')
                @component('patrocinadores.components.fields')
                    @slot('patrocinador', $patrocinador)
                @endcomponent

                <div class="row">
                    <div class="col d-grid">
                        <button class="btn btn-primary" type="submit">Actualizar</button>
                    </div>
                </div>
            @endslot
        @endcomponent

        <div class="row">
            <div class="col-12 mb-3"></div>
        </div>

        @component('components.form')
            @slot('customAttributes', 'class="d-grid p-0 col"')

            @slot('action', Route('patrocinadores.destroy', $patrocinador))

            @slot('method', "POST")

            @slot('fake_method', "DELETE")

            @slot('form_content')
                <button type="submit" class="btn btn-outline-danger">Eliminar</button>
            @endslot
        @endcomponent
    </div>

@endsection
