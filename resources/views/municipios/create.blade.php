@extends('layout.general')
@section('page-title', "Registrar Municipio")

@section('page-title-centered', 'Registrar nuevo municipio')

@section('page-content')

    <div class="create-municipio-form">
        @component('components.form')
            @slot('action', route('municipios.store'))

            @slot('method', "POST")

            @slot('form_content')
                @component('municipios.components.fields')
                @endcomponent

                <div class="row">
                    <div class="col d-grid">
                        <button class="btn btn-primary" type="submit">Registrar</button>
                    </div>
                </div>
            @endslot
        @endcomponent
    </div>

@endsection
