@extends('layout.general')

@section('page-title', 'Lista Municipios Registrados')

@section('page-title-centered', 'Lista de municipios registrados')

@section('page-content')

    <div class="row deportes-list-table">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Departamento</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($municipios as $municipio)
                    <tr>
                        <th scope="row">{{ $municipio->id }}</th>
                        <td>{{ $municipio->nombre }}</td>
                        <td>{{ $municipio->getDepartamento() }}</td>
                        <td><a href="{{ route('municipios.edit', $municipio) }}" class="btn btn-info">Editar</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection
