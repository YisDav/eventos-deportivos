@extends('layout.general')

@section('page-title', "Editar Municipio: $municipio->nombre")


@section('page-title-centered', 'Editar municipio')
@section('page-subtitle-centered', "$municipio->nombre")


@section('page-content')

    <div class="edit-municipio-form">
        @component('components.form')
            @slot('action', route('municipios.update', $municipio))

            @slot('method', "POST")

            @slot('fake_method', "PUT")

            @slot('form_content')
                @component('municipios.components.fields')
                    @slot('municipio', $municipio)
                @endcomponent

                <div class="row">
                    <div class="col d-grid">
                        <button class="btn btn-primary" type="submit">Actualizar</button>
                    </div>
                </div>
            @endslot
        @endcomponent

        <div class="row">
            <div class="col-12 mb-3"></div>
        </div>

        @component('components.form')
            @slot('customAttributes', 'class="d-grid p-0 col"')

            @slot('action', Route('municipios.destroy', $municipio))

            @slot('method', "POST")

            @slot('fake_method', "DELETE")

            @slot('form_content')
                <button type="submit" class="btn btn-outline-danger">Eliminar</button>
            @endslot
        @endcomponent
    </div>

@endsection
