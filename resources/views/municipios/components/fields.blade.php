@php
    $municipioFix = isset($municipio) ? $municipio : null;

    function getInputvalue($inputName, $municipioFix) {
        if(old($inputName) !== null) {
            return old($inputName);
        }
        else if ($municipioFix !== null) {
            return $municipioFix["$inputName"];
        }
    }
@endphp

<div class="row g-2 mb-2">
    <div class="col">
        <div class="form-floating">
            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre del municipio" value="{{ getInputvalue('nombre', $municipioFix) }}" required>
            <label for="nombre">Nombre del municipio:</label>
        </div>
    </div>
</div>

<div class="row g-2 mb-2">
    <div class="col">
        <div class="form-floating">
            <select class="form-select" id="departamento" name="departamento" required>
                <option
                    value="1"
                    @if(getInputvalue('departamento', $municipioFix) == 1)
                        selected
                    @endif>
                        Amazonas
                </option>
                <option
                    value="2"
                    @if(getInputvalue('departamento', $municipioFix) == 2)
                        selected
                    @endif>
                        Antioquia
                </option>
                <option
                    value="3"
                    @if(getInputvalue('departamento', $municipioFix) == 3)
                        selected
                    @endif>
                        Arauca
                </option>
                <option
                    value="4"
                    @if(getInputvalue('departamento', $municipioFix) == 4)
                        selected
                    @endif>
                        Atlántico
                </option>
                <option
                    value="5"
                    @if(getInputvalue('departamento', $municipioFix) == 5)
                        selected
                    @endif>
                        Bogotá
                </option>
                <option
                    value="6"
                    @if(getInputvalue('departamento', $municipioFix) == 6)
                        selected
                    @endif>
                        Bolívar
                </option>
                <option
                    value="7"
                    @if(getInputvalue('departamento', $municipioFix) == 7)
                        selected
                    @endif>
                        Boyacá
                </option>
                <option
                    value="8"
                    @if(getInputvalue('departamento', $municipioFix) == 8)
                        selected
                    @endif>
                        Caldas
                </option>
                <option
                    value="9"
                    @if(getInputvalue('departamento', $municipioFix) == 9)
                        selected
                    @endif>
                        Caquetá
                </option>
                <option
                    value="10"
                    @if(getInputvalue('departamento', $municipioFix) == 10)
                        selected
                    @endif>
                        Casanare
                </option>
                <option
                    value="11"
                    @if(getInputvalue('departamento', $municipioFix) == 11)
                        selected
                    @endif>
                        Cauca
                </option>
                <option
                    value="12"
                    @if(getInputvalue('departamento', $municipioFix) == 12)
                        selected
                    @endif>
                        Cesar
                </option>
                <option
                    value="13"
                    @if(getInputvalue('departamento', $municipioFix) == 13)
                        selected
                    @endif>
                        Chocó
                </option>
                <option
                    value="14"
                    @if(getInputvalue('departamento', $municipioFix) == 14)
                        selected
                    @endif>
                        Córdoba
                </option>
                <option
                    value="15"
                    @if(getInputvalue('departamento', $municipioFix) == 15)
                        selected
                    @endif>
                        Cundinamarca
                </option>
                <option
                    value="16"
                    @if(getInputvalue('departamento', $municipioFix) == 16)
                        selected
                    @endif>
                        Guainía
                </option>
                <option
                    value="17"
                    @if(getInputvalue('departamento', $municipioFix) == 17)
                        selected
                    @endif>
                        Guaviare
                </option>
                <option
                    value="18"
                    @if(getInputvalue('departamento', $municipioFix) == 18)
                        selected
                    @endif>
                        Huila
                </option>
                <option
                    value="19"
                    @if(getInputvalue('departamento', $municipioFix) == 19)
                        selected
                    @endif>
                        La Guajira
                </option>
                <option
                    value="20"
                    @if(getInputvalue('departamento', $municipioFix) == 20)
                        selected
                    @endif>
                        Magdalena
                </option>
                <option
                    value="21"
                    @if(getInputvalue('departamento', $municipioFix) == 21)
                        selected
                    @endif>
                        Meta
                </option>
                <option
                    value="22"
                    @if(getInputvalue('departamento', $municipioFix) == 22)
                        selected
                    @endif>
                        Nariño
                </option>
                <option
                    value="23"
                    @if(getInputvalue('departamento', $municipioFix) == 23)
                        selected
                    @endif>
                        Norte de Santander
                </option>
                <option
                    value="24"
                    @if(getInputvalue('departamento', $municipioFix) == 24)
                        selected
                    @endif>
                        Putumayo
                </option>
                <option
                    value="25"
                    @if(getInputvalue('departamento', $municipioFix) == 25)
                        selected
                    @endif>
                        Quindío
                </option>
                <option
                    value="26"
                    @if(getInputvalue('departamento', $municipioFix) == 26)
                        selected
                    @endif>
                        Risaralda
                </option>
                <option
                    value="27"
                    @if(getInputvalue('departamento', $municipioFix) == 27)
                        selected
                    @endif>
                        San Andrés y Providencia
                </option>
                <option
                    value="28"
                    @if(getInputvalue('departamento', $municipioFix) == 28)
                        selected
                    @endif>
                        Santander
                </option>
                <option
                    value="29"
                    @if(getInputvalue('departamento', $municipioFix) == 29)
                        selected
                    @endif>
                        Sucre
                </option>
                <option
                    value="30"
                    @if(getInputvalue('departamento', $municipioFix) == 30)
                        selected
                    @endif>
                        Tolima
                </option>
                <option
                    value="31"
                    @if(getInputvalue('departamento', $municipioFix) == 31)
                        selected
                    @endif>
                        Valle del Cauca
                </option>
                <option
                    value="32"
                    @if(getInputvalue('departamento', $municipioFix) == 32)
                        selected
                    @endif>
                        Vaupés
                </option>
                <option
                    value="33"
                    @if(getInputvalue('departamento', $municipioFix) == 33)
                        selected
                    @endif>
                        Vichada
                </option>
            </select>
            <label for="departamento">Departamento:</label>
        </div>
    </div>
</div>
